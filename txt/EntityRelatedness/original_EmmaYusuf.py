# Implementation of some ways to do intrinsic evaluation
# by analysing the relational aspect of the embedding.
# Yusuf Kocak and Emma Ahrens, August 2018

import pandas as pd
import numpy as np
from scipy import spatial
from scipy import stats
np.set_printoptions(precision=20)

# Calculate the absolute semantic error and the semantic
# transition distance similar to the description in
# https://arxiv.org/abs/1803.04488 (Concept2vec: Metrics for
# Evaluating Quality of Embeddings for Ontological Concepts)
# for the data in the KORE dataset

# Get the data from KORE_data.csv
filename = 'data/KORE_data.csv'
data = pd.read_csv(filename, converters={
        'vec': lambda x: np.fromstring(x[1:-1], sep=' ')
})

# Semantic relatedness metric or absolute semantic error
# We calculate delta(e_i, e_j) = |s'(e_i, e_j) - s(V_{e_i}^t, V_{e_j}^t)|
# for entities in the same way that the paper calculates the distance
# between concepts. Note that s' is the distance evaluated by human judges
# and s ist the cosine similarity.
# The KORE dataset provides us with 20 entities per "main" entity (in total 420)
# that are ranked by there proximity to the "main" entity.
# To receive the error we are calculating the cosine similarity between
# every entity and the main entity. Than we rank the entities by the cosine
# similarity and calculate the kendall tau error between the original ranking
# and the ranking provided by the cosine similarity.

def compute_similarity(row):
    main = data[data['name']==row['relates_to']]
    # The vector for the main entity "Deus Ex (video game)" does not
    # exist, so we need to exclude these cases.
    if len(main) != 0:
        main = main.iloc[0]['vec']
        current = row['vec']
        return np.abs(-spatial.distance.cosine(main, current)+1)
    return 1

data['similarity'] = data.apply(compute_similarity, axis=1)

main_entities = data['relates_to'].unique()

errors = []
for main in main_entities:
    if main != "Deus Ex (video game)":
        related = data[data['relates_to']==main]
        related_cosine = related.sort_values(by='similarity', ascending=True, inplace=False)
        related_human = related.sort_values(by='rank', ascending=True, inplace=False)
        related_cosine = related_cosine['rank'].tolist()
        related_human = related_human['rank'].tolist()
        tau, _ = stats.kendalltau(related_cosine, related_human)
        errors.append({
            'tau': np.abs(tau),
            'topic': main
        })
errors = pd.DataFrame(errors)

print('tau errors:')
print(errors)
average_error = np.mean(np.vstack(errors['tau'].values), axis=0)
print('average:')
print(average_error)