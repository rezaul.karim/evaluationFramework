import numpy as np;

file = open("/home/garpel/evaluation/dbpediaVectors/inversePageRankSplit11.txt","w",encoding='ISO-8859-1')

embeddings = open("/data/biasedRDF2VecRecursive/data.dws.informatik.uni-mannheim.de/rdf2vec/models/DBpedia/2016-04/Biased/InversePageRankSplit11/db2vec_sg_200_5_25_5.txt","r",encoding='ISO-8859-1')

for line in embeddings:
    wordAndVector = line.split('\t', 1)
    word = wordAndVector[0]
    vector = wordAndVector[1]
    #embedding_vector = np.asarray(vector, dtype='float32')
    file.write(word[1:-1] + "\t" + vector)


embeddings.close()
file.close()
