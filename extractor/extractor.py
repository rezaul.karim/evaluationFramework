import gensim;
import numpy as np;

#file = open("/home/garpel/evaluation/dbpediaVectors/pageRank.txt","w",encoding='ISO-8859-1')
file = open("/home/garpel/evaluation/dbpediaVectors/inversePageRank.txt","w")

#load embeddings binary model with gensim for word2vec and rdf2vec embeddings
model = gensim.models.Word2Vec.load("/data/garpel/rdf2vec_inversePageRank/db2vec_sg_200_5_25_5")
embedded_entities = model.wv

#file.write(str(embedded_entities))

vocab = model.wv.vocab

for word in vocab:

        vector = embedded_entities.get_vector(word)
        vector_string = ""
        for field in vector:
                vector_string += str(float(field))+'\t'

        file.write(word[1:-1] + "\t" + vector_string + "\n")
        #file.write(word)
        #file.write(embedded_entities.get_vector(word))

file.close()

